from flask import Flask, request
from config import token
import requests
import sqlite3
import random

app = Flask(__name__)

url = 'https://api.telegram.org/bot{0}/sendMessage'.format(token)

conn = sqlite3.connect('db.sqlite3', check_same_thread=False)
curs = conn.cursor()


def get_id(json):
    return json['message']['chat']['id']


def get_text(json):
    return json['message']['text']


def add_user(chat_id):
    user = curs.execute("SELECT chat_id FROM users WHERE chat_id={0}".format(chat_id)).fetchone()
    if user is None:
        curs.execute(
            "INSERT INTO users VALUES ({0}, {1}, '{2}')".format(chat_id, False, None))
        conn.commit()


def send_mess(chat_id, mess, names_btn=[]):
    if len(names_btn) == 0:
        response = {'chat_id': chat_id, 'text': mess}
        requests.post(url=url, json=response)
    elif len(names_btn) != 0:
        buttons = [[{'text': names_btn[0]}]]
        response = {'chat_id': chat_id, 'text': mess,
                    'reply_markup': {'keyboard': buttons, 'resize_keyboard': True}}
        requests.post(url=url, json=response)


@app.route('/', methods=['POST', 'GET'])
def text():
    if request.method == "POST":
        json = request.get_json()
        if json.get('message'):
            chat_id = get_id(json)
            if json.get('message').get('text'):
                txt = get_text(json)
            else:
                send_mess(chat_id, 'Введен неккоректный текст')
                return {'ok': True}
        else:
            return {'ok': True}

        # Команда /start
        if txt == '/start':
            send_mess(chat_id=chat_id, mess='Привет {0} ты запустил ChatterBot\n\n'
                                            'Моя задача найти тебе собеседника\n'
                                            'А дальше решай сам ОБЩАТЬСЯ или НЕТ\n\n'
                                            'Удачи в использовании ☺'.format(json['message']['chat']['first_name']),
                      names_btn=['Нaйти собеседника'])
            add_user(chat_id)

        # Найти собеседника
        elif txt == 'Нaйти собеседника':
            curs.execute("UPDATE users SET online={0} WHERE chat_id={1}".format(True, chat_id))
            conn.commit()
            send_mess(chat_id, 'Поиск собеседника в процессе...', ['Oтмена'])
            ids = curs.execute("SELECT chat_id FROM users WHERE online={0}".format(True)).fetchall()
            for l1, l2 in zip(range(0, len(ids), 2), range(1, len(ids), 2)):
                curs.execute("UPDATE users SET online={0}, friend_id={1} WHERE chat_id={2}".format(False, ids[l2][0],
                                                                                                   ids[l1][0]))
                curs.execute("UPDATE users SET online={0}, friend_id={1} WHERE chat_id={2}".format(False, ids[l1][0],
                                                                                                   ids[l2][0]))
                conn.commit()
                send_mess(ids[l1][0], 'Собеседник найден!!\nМожете начинать общаться', ['Пoкинуть собеседника'])
                send_mess(ids[l2][0], 'Собеседник найден!!\nМожете начинать общаться', ['Пoкинуть собеседника'])

        elif txt == 'Oтмена':
            send_mess(chat_id, 'ОК', ['Нaйти собеседника'])
            curs.execute("UPDATE users SET online={0} WHERE chat_id={1}".format(False, chat_id))
            conn.commit()

        elif txt == 'Пoкинуть собеседника':
            send_mess(chat_id, 'Вы покинули собеседника', ['Нaйти собеседника'])
            id_friend = curs.execute("SELECT friend_id FROM users WHERE chat_id={0}".format(chat_id)).fetchone()[0]
            send_mess(id_friend, 'Собеседник покинул вас', ['Нaйти собеседника'])
            curs.execute("UPDATE users SET friend_id='{0}' WHERE chat_id={1}".format(None, chat_id))
            curs.execute("UPDATE users SET friend_id='{0}' WHERE chat_id={1}".format(None, id_friend))
            conn.commit()

        elif True:
            friend_id = curs.execute("SELECT friend_id FROM users WHERE chat_id={0}".format(chat_id)).fetchone()[0]
            send_mess(friend_id, txt)

    return {'ok': True}


app.run(host='192.168.1.100', port=5002)
